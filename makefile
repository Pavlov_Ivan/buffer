CC=g++
CFLAGS=-std=c++11

default: main.o buffer.o
	$(CC) $(CFLAGS) buffer.o main.o -o buffer

main.o: main.cpp
	$(CC) $(CFLAGS) -c main.cpp

buffer.o: buffer.cpp
	$(CC) $(CFLAGS) -c buffer.cpp

