#include<iostream>
#include<cstring>
#include "buffer.hpp"

using namespace std;
using namespace bicycle;

int main()
{
	cout << unitbuf; 
	buffer b;	
	const char* buf = "Hello World!";
	size_t n = strlen(buf);//sizeof(buf);
	size_t c = strlen(buf) + 10;
	cout<<b<<endl;

	buffer b1(n);
	memcpy(b1.data(), buf, n);
	cout<<b1<<endl;

	buffer b2(0, 1024);
	b2.size(n);
	memcpy(b2.data(), buf, n);
	cout<<b2<<endl;

	buffer b3(1024, 5 * 1024);
	memset(b3.data(), '?', 1024);
	cout<<b3<<endl;

	for(int i = 0; i < 5; ++i)
		b3.append(buf, n);
	cout<<b3<<endl;


	if(b3.capacity() < 10 * 1024)
		b3.capacity(10 * 1024);
	cout<<b3<<endl;

	buffer b4(buf, n);
	cout<<b4<<endl;

	buffer b5(buf, n, 5 * 1024);
	cout<<b5<<endl;
	
	//buffer b6(buf, n, n, true);	
	return 0;
}
