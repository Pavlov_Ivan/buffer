#ifndef BUFFER_H_
#define BUFFER_H_

namespace bicycle {

class buffer
{
public:
	buffer();
	buffer(int sz);
	buffer(int sz, int capacit);
	buffer(const char* buf, int sz);
	buffer(const char* buf, int sz, int capacit);
	~buffer();
	void copy(const char* buf, int n);
	char* data() const;
	void append(const char* buf, int n);
	int capacity() const;
	void capacity(int n);
	int size() const;
	void size(int);
private:
	char* data_;
	int size_;
	int capacity_;
};

std::ostream& operator<<(std::ostream& os, const buffer& b);

}

#endif //BUFFER_H_
