#include<iostream>
#include<cstdlib>
#include<cstring>
#include "buffer.hpp"

namespace bicycle {

void buffer::capacity(int n)
{
	if(n < 0)
		return ;
	if(n < size_)
		n = size_;
	data_ = (char*)std::realloc((void*)data_, n);
	capacity_ = n;
}

int buffer::capacity() const
{
	return capacity_;
}

int buffer::size() const
{
	return size_;
}

void buffer::size(int n)
{
	if(n < 0)
		return ;
	if(n > capacity_)
	{
		capacity(n);
	}
	size_ = n;
}

void buffer::append(const char* buf, int n)
{
	if(n <= 0 || NULL == buf)
		return ;
	int old_size_ = size_;
	size(size_ + n);
	std::memcpy((void*)(data_ + old_size_), (void*)buf, n);
}

void buffer::copy(const char* buf, int n)
{
	size(0);
	append(buf, n);
}

char* buffer::data() const
{
	return data_;	
}

buffer::buffer()
{
	data_ = NULL;
	size_ = 0;
	capacity_ = 0;
}

buffer::buffer(int sz, int capacit): buffer()
{
	capacity(capacit);
	size(sz);
}

buffer::buffer(int sz): buffer(sz, sz)
{}

buffer::buffer(const char* buf, int sz, int capacit): buffer(sz, capacit)
{
	copy(buf, sz);
}

buffer::buffer(const char* buf, int sz): buffer(buf, sz, sz)
{}

buffer::~buffer()
{
	free(data_);
}

std::ostream& operator<<(std::ostream& os, const buffer& b)
{
	const char *ptr = NULL==b.data()?"NULL":b.data();
	os	<<"data "	<<ptr		<<std::endl
		<<"size "	<<b.size()	<<std::endl
		<<"capacity "	<<b.capacity()	<<std::endl;
	return os;
}

}
